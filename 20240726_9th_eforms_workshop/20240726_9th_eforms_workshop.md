---
title: 9th eForms Workshop
author: DG GROW.G4
date: 26 July 2024
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Agenda 

The 6th eForms Workshop takes place on the **26 July 2024**, **from 10:000 to 12:00**.

* 10:00 – 10:10: Introduction
* 10:10 – 11:00: eForms Guide on Review
* 11:00 – 11:15: Updated eForms Guide on EED
* 11:15 – 11:30: eForms Guide on IPI
* 11:30 – 11:40: Voluntary Forms 
* 11:40 – 11:50: AOB
* 11:50 – 12:00: Closing

# Introduction

## Etiquette for all participants

1. During the presentation, keep your **mic off** and **video off**.
2. Use the **chat** to ask your questions.
3. The session will **not be recorded**.
4. Don't be shy to **participate actively** throughout the session.
5. Please **raise your hand** if you wish to take the floor.

## Organisational aspects

* When you log into GitLab the first time with your EU-Login, you cannot create issues or comments immediately. Instead, you might receive first an eMail from the owner of GitLab to write him back. Only then you will be granted write access. If the issue persists, please contact us by <grow-eprocurement@ec.europa.eu>.
* As it is not always possible that all can participate, please try to get a substitute for you.
* The meeting minutes from the 7th eForms Workshop are available [here](https://code.europa.eu/eproc/eforms/workshops/-/blob/main/20240621_7th_eforms_workshop/20240621_7th_eforms_workshop_minutes.md) and the PDF version [here](https://code.europa.eu/eproc/eforms/workshops/-/blob/main/20240621_7th_eforms_workshop/20240621_7th_eforms_workshop_minutes.pdf). Do you have any comments?
* The meeting minutes from the 8th eForms Workshop are available [here](https://code.europa.eu/eproc/eforms/workshops/-/blob/main/20240704_8th_eforms_workshop/20240704_8th_eforms_workshop_minutes.md) and the PDF version [here](https://code.europa.eu/eproc/eforms/workshops/-/blob/main/20240704_8th_eforms_workshop/20240704_8th_eforms_workshop_minutes.pdf). Do you have any comments?
* Is the agenda okay for today?

# eForms Guide on Review

The guide on Review is available. We will discuss this proposal during this eForms Workshop. While working on the guide we updated the fields to capture the different stages of the review procedure (appeals of first instance review decision).  The fields will allow to capture more precisely the exact stage and status of the review procedure.

* You can find the web version of the guide here: [https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_005_rew.md](https://code.europa.eu/eproc/eforms/docs/-/commit/fe5c2a0dca4160b59f230b69bed59a6219e4ea1f)

* The PDF version is here: https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_005_rew.pdf

If you have comments, looking beyond SDK 1.13., please use this issue [crs#63](https://code.europa.eu/eproc/eforms/crs/-/issues/63)

# Updated eForms Guide on EED

Based on the feedback received during the 8th eForms workshop, we updated the guide to reflect the fields mentioned in the 2nd amendment.  We will go over the updated EED guide during the workshop to close this topic for SDK 1.13. implementation.

* You can find the web version of the guide here: https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_004_eed.md 
* The PDF version is here: https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_004_eed.pdf 
* It is important to mention that we will continue to work on the EED guide with you and DG ENER in the upcoming workshops, looking beyond SDK 1.13. If you have comments, please use this issue [crs#59](https://code.europa.eu/eproc/eforms/crs/-/issues/59)

# eForms Guide on IPI

Based on the feedback received the IPI guide has been updated. The code list will be updated when a specific IPI measure is introduced through an Implementing Act, currently there are no such Act available as such the code list is empty.

* You can find the web version of the guide here: https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_002_ipi.md 
* The PDF version is here: https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_002_ipi.pdf

Feedback period is closed for IPI Guide, the feedback shared are available under issue [crs#10.](https://code.europa.eu/eproc/eforms/crs/-/issues/10)

# Voluntary forms

 Based on the feedback received during the 7th eForms workshop, the voluntary forms will be based on the classical Directive 2014/24/EU. For the implementation of the SDK 1.13 the most important points: 

* For the field `Procedure Legal Basis (BT-01)` countries can tailor the code list to reflect their own national needs. It is also important to highlight that next to the predefined list it is possible to provide the national legislation under `Procedure BT-01(d)` text field.
* For Defence, the contract modification notice will be a copy of type 38. E6 will be independent on this. The reason is, that we will be more flexible with E6.

To close the discussion, it is important to agree if there is a need to add: 

* Something like `None` to the code list for those countries where no legislative framework exist? Please be reminded that `Other` could cover this also.
* How could we identify the national procedures for those countries that always select one of the Directives for below the threshold procedure?

Feedback period is closed for Guide on Voluntary Forms, the feedback shared are available under issue: [crs#40](https://code.europa.eu/eproc/eforms/crs/-/issues/40)

We will update the Annex as we go along. In this way it is easy for all to see the changes.

# AOB

* Are there other issues that you would like to discuss?

# Closing

## Next steps

* We will share with you the minutes of the 9th eForms workshop.
* We will work on the eForms Governance Document.

## Upcoming events

* **24 September:** PPDS Day 2024 – The Grand Opening: The European Commission is hosting a full–day event celebrating the go–live of the Public Procurement Data Space (PPDS)! The event will include live demonstrations of the PPDS, discuss what’s in it for users and the motivations for countries to join the PPDS and share their data. In parallel sessions, participants will learn how to use the PPDS, but also about how procurement data is assessed in terms of their data quality along with insights into the technical aspects of the PPDS. A networking cocktail will conclude the day. Please note that this is not a hybrid event. You can register for the event via this [link.](https://single-market-economy.ec.europa.eu/events/ppds-day-2024-grand-opening-2024-09-24_en#:~:text=On 24 September%2C the European,attend the PPDS Day 2024.)
* **25 September:** PPDS Community Workshop. We still need to find a venue place for it. 

## Feedback

* How did you like this workshop?
* What can we improve?