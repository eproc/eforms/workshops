---
title: 8th eForms Workshop
author: DG GROW.G4
date: 04 July 2024
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Agenda 

The 6th eForms Workshop takes place on the **04 July 2024**, **from 09:30 to 12:00**.

* 09:30 – 09:40: Introduction
* 09:40 – 11:40: EED
* 11:40 – 11:50: AOB
* 11:50 – 12:00: Closing

# Introduction

## Etiquette for all participants

1. During the presentation, keep your **mic off** and **video off**.
2. Use the **chat** to ask your questions.
3. The session will **not be recorded**.
4. Don't be shy to **participate actively** throughout the session.
5. Please **raise your hand** if you wish to take the floor.

## Organisational aspects

* When you log into GitLab the first time with your EU-Login, you cannot create issues or comments immediately. Instead, you might receive first an eMail from the owner of GitLab to write him back. Only then you will be granted write access. If the issue persists, please contact us by <grow-eprocurement@ec.europa.eu>.
* As it is not always possible that all can participate, please try to get a substitute for you.
* The meeting minutes from the 7th eForms Workshop are available [here](https://code.europa.eu/eproc/eforms/workshops/-/blob/main/20240621_7th_eforms_workshop/20240621_7th_eforms_workshop_minutes.md) and the PDF version [here](https://code.europa.eu/eproc/eforms/workshops/-/blob/main/20240621_7th_eforms_workshop/20240621_7th_eforms_workshop_minutes.pdf). If you have any comments please write to us under grow-eprocurement@ec.europa.eu until 15 July 2024.
* Is the agenda okay for today?

# EED

The guide on EED is available. We will discuss this proposal during this eForms Workshop. While working on the guide it became clear that we need to add more fields to be able to understand what the buyer requested and what was in the end procured. DG ENER provides a website that will also support buyers. General information can be found here https://energy-efficient-products.ec.europa.eu/ecodesign-and-energy-label_en. Besides this, DG ENER provides also a service where information on labelling can be found for products itself https://eprel.ec.europa.eu/screen/home.

* You can find the web version of the guide here: https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_004_eed.md
* The PDF version is here: https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_004_eed.pdf

If you have comments, please use this issue https://code.europa.eu/eproc/eforms/crs/-/issues/59

# AOB

* Please provide you feedback on how you have implemented the voluntary eForms under [issue #40](https://code.europa.eu/eproc/eforms/crs/-/issues/40) until 15 July.
* We will work on a governance framework for eForms during Summer.
* Are there other issues that you would like to discuss?

# Closing

## Next steps

* We will share with you the minutes of the 8th eForms workshop.
* We will work on a guide on Review.

## Upcoming events

* **24 September:** PPDS Day 2024 – The Grand Opening: The European Commission is hosting a full–day event celebrating the go–live of the Public Procurement Data Space (PPDS)! The event will include live demonstrations of the PPDS, discuss what’s in it for users and the motivations for countries to join the PPDS and share their data. In parallel sessions, participants will learn how to use the PPDS, but also about how procurement data is assessed in terms of their data quality along with insights into the technical aspects of the PPDS. A networking cocktail will conclude the day. Please note that this is not a hybrid event. You can register for the event via this [link.](https://single-market-economy.ec.europa.eu/events/ppds-day-2024-grand-opening-2024-09-24_en#:~:text=On 24 September%2C the European,attend the PPDS Day 2024.)
* **25 September:** PPDS Community Workshop. We still need to find a venue place for it. 

## Feedback

* How did you like this workshop?
* What can we improve?