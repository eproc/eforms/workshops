---
title: 8th eForms Workshop Minutes
author: DG GROW.G4
date: 04 July 2024
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
 \usepackage{plex-serif}
 \usepackage[left]{lineno}
 \linenumbers
---

# Photo

![Photo of participants participating virtually](20240704_8th_eforms_workshop_photo.png)

# Key takeaways on the discussion on the EED Guide

* EC presented a draft guide on the EED fields, proposing new fields to make the reporting obligation easier for the buyers.
* The Member States have highlighted that they are currently transposing the EED. As such it would be difficult to provide feedback for the proposed fields now, and more time would be needed to understand the fields in detail. 
* When it comes to including the newly proposed fields (optionally) in SDK1.13, some Member States have raised that as these fields are not part of the 2nd amendment, even if they are optional, they will not implement them on the technical side without a legal change (3rd amendment). Prior to including these elements in the technical implementation, Member States should be consulted within the framework of the amendment procedure.
* The topic of sectorial legislations was also discussed, some Member States asked the Commission to have a detailed governance on how the sectorial legislation should be included eForms. 
* The Commission will discuss internally the above points and come back with an update guide and timeline for the EED discussions.

# Protocol

## Guide on EED

* from Duran Alp DE - Adesso SE to everyone:  9:38 AM
  * Did we have access to the presentation?
* from Zsófia Dudás - DG GROW to everyone:  9:38 AM
  * We will share the presentation after the workshop.
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  9:53 AM
  * So the impact of the object of the procurement is left to the buyer's discretion, right?
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  9:53 AM
  * Don't you think that buyers might be a bit lost here?
* from FI - Timo Rantanen to everyone:  9:54 AM
  * A bit!! :-)
* from Andjelka Petreski NL to everyone:  9:56 AM
  * Our question was: We understood from a webinar meeting with COM I do not know if Heidi was present, that the EED only applies above EU threshold only for purchases with an energy component. :-) For example, if we are purchasing a cow, there might not be an energy component
* From DG ENER: 
  * What you said is true, you can only make an impact transparent if the purchase is somehow related to energy and has an energy impact. As per your example of purchasing (of a cow) with no energy impact, EED is still applicable as it has other provisions in article 7, in this case you have to select no EED impact. In recap, article 7 applies to the purchase, but as there is no EED impact it is not necessary to include any other information in the notices. Adding to this yes, the requirement to purchase only products, services buildings and works with high energy-efficiency performance applies for concluding public contracts and concessions with a value equal to or greater than the thresholds laid down Article 8 of Directive 2014/23/EU, Article 4 of Directive 2014/24/EU and Article 15 of Directive 2014/25/EU
* from Savina Kalanj AT to everyone:  9:56 AM
  * What is the planned process? Most of the suggested fields are not contained in the Implementing Regulation. Our understanding is that the energy efficiency first principle (which is broader than procurement with an energy component) has to be considered ex ante and may have an effect on what is procured.
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  10:00 AM
  * Totally support Savina's points. On the "optional" nature of these fields, the approach" paragraph of the guide tends to say that they are "currently not mandatory"
* EC- DG GROW:
  * We wanted to see your view on the possibility of including these new fields in SDK 1.13 as optional. This would provide a possibility for Member States to test these fields prior to the EED becoming mandatory in October 2025. 
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  10:05 AM
  * France will not agree either.
* from Alexandra Rodrigues PT to everyone:  10:06 AM
  * PT is with AT and FR, will not agree either
* from IE-Nora Bermingham to everyone:  10:08 AM
  * IE needs time to consider this proposal but at this time our preference is to hold off on including the proposed changes in SDK 1.13 and wait for the 3rd amendment to be agreed. The legal basis and also the additional technical implementation time on our side are concerns. 
* from FI - Timo Rantanen to everyone:  10:17 AM
  * CPV code 4500000 is very commonly used for works contracts. If the works is about buildings, the construction work includes also appliances, HVAC stuff etc which all have energy efficiency impact. I do not see a way to use CPV codes in eproc systems in identifying EED in a procurement
* from Andjelka Petreski NL to everyone:  10:17 AM
  * Just like Ireland we cannot give our approval to the proposal as we need to consider it. We share concerns about the legal aspects but also, we are in the process of transposing the directive and this discussion will come too soon as well. We just received the guidance document
* from Savina Kalanj AT to everyone:  10:18 AM
  * The question is what the impact is to be shown on - the main subject matter of the contract or any small component? We think it is the former
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  10:26 AM
  * In our opinion, the constant addition of new fields related to sectoral files is also a hindrance to the understanding and readability of public procurement legislation, particularly from the point of view of buyers.
* from IE-Nora Bermingham to everyone:  10:31 AM
  * IE also shares concerns on admin burden for buyers - the 2nd amendment requirements need time to embed with the buyers.
* from EC - Marc Christopher SCHMIDT to everyone:  10:36 AM
  * How could the impact be provided and measured?
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  10:38 AM
  * No clue on this. We have a dedicated team here which is working on the transposition and we lacked time to discuss the guide with them and I cannot tell where they are on the transposition journey
* from IE-Nora Bermingham to everyone:  10:38 AM
  * Not in a position to address this question today - would need to consult with colleagues dealing with implementation of Art 7
* from Alexandra Rodrigues PT to everyone:  10:38 AM
  * PT needs more time to think about it
* from Savina Kalanj AT to everyone:  10:38 AM
  * We also need to analyse guidance and start transposition before we can respond meaningfully
* from Andjelka Petreski NL to everyone:  10:39 AM
  * same as Austria
* from DK Simon BUSCH to everyone:  10:39 AM
  * As others, IE PT, AT.
* from Matija Karaula to everyone:  10:39 AM
  * same as AT
* from Andjelka Petreski NL to everyone:  10:40 AM
  * How realistically is implementation in 1.13 considering this discussion? 
* from Savina Kalanj AT to everyone:  10:40 AM
  * Any fields that are implemented have to comply with the Implementing Regulation - i.e., no added fields
* from Andjelka Petreski NL to everyone:  10:41 AM
  * perhaps you need to think about this as well :) 
* from Matija Karaula to everyone:  10:41 AM
  * if they are optional, no one will use them
* from Duran Alp DE - Adesso SE to everyone:  10:41 AM
  * it also depended how the country implements this
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  10:42 AM
  * Mandatory or CM?
* from Duran Alp DE - Adesso SE to everyone:  10:42 AM
  * in Germany we are tailoring the rules
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  10:43 AM
  * 2 rounds before the integration of these fields in a 3rd amendment?
* from Savina Kalanj AT to everyone:  10:44 AM
  * What about governance/EXPP?
* from Savina Kalanj AT to everyone:  10:51 AM
  * I am not sure we will have input ready
* from IE-Nora Bermingham to everyone:  10:51 AM
  * IE can support this end of July mtg suggestion - but will need to verify with colleagues
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  10:52 AM
  * the implementation process is still ongoing on our side, so I don't know to what extent we'll be able to contribute qualitatively to these discussions, and whether we can even validate the results of these discussions 
* from Andjelka Petreski NL to everyone:  10:53 AM
  * We are ofc happy to share our experiences and views, but I do have concerns that July may be too soon. 
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  10:53 AM
  * we have a dedicated team on the procurement part of the EED but I'm not sure they are ready to provide contributions
* from Andjelka Petreski NL to everyone:  10:53 AM
  * ofc = of course
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  10:54 AM
  * will see with them though
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:  10:59 AM
  * I agree with AT. We need to discuss the EED fields with the right governance model at 3rd amendment workshop/EXEP/ACPC and the right calendar

## Closing

### Farewell

* from Savina Kalanj AT to everyone:    11:05 AM
  * Thank you for the useful discussion! EED is a tough one :) Please give us the information earlier next time (as Andjelka said)
* from Pani Koullourou to everyone:    11:06 AM
  * thank you!
* from IE-Nora Bermingham to everyone:    11:07 AM
  * Useful meeting - discussions very helpful and openness appreciated
* from Duran Alp DE - Adesso SE to everyone:    11:07 AM
  * Bye
* from Andjelka Petreski NL to everyone:    11:07 AM
  * see you!
* from Jason Grech to everyone:    11:07 AM
  * Thanks
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:    11:07 AM
  * I like the way we can work on the next amendment with guidance as supportive documents. Would like the priority to be on the amendment with the right governance.
* from LU - Marc Nosbusch to everyone:    11:07 AM
  * thank you and bye
* from Pierre HOULLEMARE FR - Ministry of Economy to everyone:    11:07 AM
  * Bye
* from Matija Karaula to everyone:    11:07 AM
  * bye!
* from SI-Ajda Kostanjsek to everyone:    11:07 AM
  * Thank you and bye.
* from Cátia Miguel_PT to everyone:    11:07 AM
  * tank you
* from DK Simon BUSCH to everyone:    11:07 AM
  * Bye thanks!
* from BE - Michaël De Winne BOSA to everyone:    11:07 AM
  * Bye!
* from PT - Sandra Mascarenhas to everyone:    11:07 AM
  * thank you!!
* from LU - Strobant Adrien to everyone:    11:07 AM
  * bye
* from Alexandra Rodrigues PT to everyone:    11:07 AM
  * Thank you all. It was a very good discussion
* from Carmen Collantes to everyone:    11:07 AM
  * Thank you!

