---
title: 7th eForms Workshop
author: DG GROW.G4
date: 21 June 2024
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Agenda 

The 6th eForms Workshop takes place on the **21 June 2024**, **from 13:00 to 15:00**.

* 13:00 – 13:10: Introduction
* 13:10 – 14:00: Voluntary forms
* 14:00 – 14:40: Other issues
* 14:40 – 14:50: AOB
* 14:50 – 15:00: Closing

# Introduction

## Etiquette for all participants

1. During the presentation, keep your **mic off** and **video off**.
2. Use the **chat** to ask your questions.
3. The session will **not be recorded**.
4. Don't be shy to **participate actively** throughout the session.
5. Please **raise your hand** if you wish to take the floor.

## Organisational aspects

* When you log into GitLab the first time with your EU-Login, you cannot create issues or comments immediately. Instead, you might receive first an eMail from the owner of GitLab to write him back. Only then you will be granted write access. If the issue persists, please contact us by <grow-eprocurement@ec.europa.eu>.
* As it is not always possible that all can participate, please try to get a substitute for you.
* Any comments on the minute meetings from the last eForms workshop?
* Is the agenda okay for today?

# Voluntary forms

> “Contracting authorities may publish notices for public contracts that are not subject to the publication requirement laid down in this Directive provided that those notices are sent to the Publications Office of the European Union by electronic means in accordance with the format and procedures for transmission indicated in Annex VIII.” - Art 51(6) of Directive 24/2014

## The 6 voluntary forms

* **E1** Pre-Market Consultation and **E2** Prior Information Notice will be derived from eForms notice type 4 (Prior Information Notice - Directive 2014/24/EU).
* **E3** Contract Notice will be derived from eForms notice type 16 (Contract Notice - Directive 2014/24/EU).
* **E4** Contract Award Notice and **E5** Contract Completion will be derived from eForms notice type 29 (Contract Award - Directive 2014/24/EU)
* **E6** Contract Modification will be derived from notice type 38 (Contract Modification - Directive 2014/24/EU)

## Overview from countries using the voluntary forms

Overview of the possible use of voluntary forms in the Member States (based on the feedback received during the 7th eProcurement workshop).

![Use of voluntary in countries](20240621_7th_eforms_workshop_voluntary_forms_overview.png)

## Topics to discuss

### Group Framework values

The use of “Group framework values” is possible in E1 and E2 according to the Annex. Technically this would be challenging. Our proposal is therefore to remove this from the two forms.This would make also the E1 and E2 easier to use.

![Screenshot of the Annex on the group framework values](20240621_7th_eforms_workshop_group_framework_values.png)

### Other topics on voluntary forms

* We would like to remove lots from E1 and E2 altogether, because even for the other PINs we do not use lots.
* For contract modification we would have a dedicated form for Directive 2009/81/EC and keep E6 for below the threshold only. This would make E6 also easier.
* We would like to use E4 also for reporting purposes.
* Are there fields not mentioned, but we should include?
* Do you have any other feedback on the voluntary forms?

### Communication

* So far, we have only received input from Belgium. If others would like to share their work, please provide them under [issue #40](https://code.europa.eu/eproc/eforms/crs/-/issues/40) until 15 July.
* We will update the Annex as we go along. In this way it is easy for all to see the changes.

# Other issues

* Multiple lots award notices issues ([issue #51](https://code.europa.eu/eproc/eforms/crs/-/issues/51))
* Competition is stopped due to appeal ([issue #12](https://code.europa.eu/eproc/eforms/crs/-/issues/12)) 
* Some feedback on Review BG-716
* Do you have any other issues you want to discuss?

# AOB

* Priority timeline for SDK 1.13
  * Review
  * Voluntary forms
  * EED and IPI
  * Other
* Are there other issues that you would like to discuss?

# Closing

## Next steps

* We will share with you the minutes of the 7th eForms workshop.
* Information on the Energy Efficiency Directive fields will be provided on Gitlab before the 8th eProcurement Workshop.
* We will also work on a guide on Review.

## Upcoming events

* **27 June:** Co-creation session on the Public Procurement Data Space (PPDS): It will take place on Thursday, June 27, from 10:00 to 12:30 CET. It will include a live guided tour of the PPDS platform and to discuss about latest advancements and upcoming steps.
* **4 July:** 8th eForms workshop on the fields regarding Energy Efficiency Directive: We will discuss the fields on the Energy Efficiency Directive on the 4 July from 10:00 to 12:00 CET. It is important for us to get your feedback as these fields will become mandatory to use from October 2025. The meeting is organised online. We plan also to discuss the topic on Review in this workshop.
* **24 September:** PPDS Day 2024 – The Grand Opening: The European Commission is hosting a full–day event celebrating the go–live of the Public Procurement Data Space (PPDS)! The event will include live demonstrations of the PPDS, discuss what’s in it for users and the motivations for countries to join the PPDS and share their data. In parallel sessions, participants will learn how to use the PPDS, but also about how procurement data is assessed in terms of their data quality along with insights into the technical aspects of the PPDS. A networking cocktail will conclude the day. Please note that this is not a hybrid event. You can register for the event via this [link.](https://single-market-economy.ec.europa.eu/events/ppds-day-2024-grand-opening-2024-09-24_en#:~:text=On 24 September%2C the European,attend the PPDS Day 2024.)

## Feedback

* How did you like this workshop?
* What can we improve?