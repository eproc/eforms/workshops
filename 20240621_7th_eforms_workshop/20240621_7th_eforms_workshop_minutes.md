---
title: 7th eForms Workshop Minutes
author: DG GROW.G4
date: 21 June 2024
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
 \usepackage{plex-serif}
 \usepackage[left]{lineno}
 \linenumbers
---

# Photo

![Photo of participants participating virtually](20240621_7th_eforms_workshop_photo.png)

# Key takeaways for voluntary forms

* Has the highest priority over all other aspects foreseen for SDK 1.13.
* The voluntary forms will be based on the classical Directive 2014/24/EU.
* `Procedure Legal Basis (BT-01)` should include next to `other` also all Directives because in some countries they are used also as a legal basis for below the EU thresholds. As there is no legal basis for below publication in the Netherlands, `other` might not be sufficient. Therefore, another code might be added like `None`. It is important to note that it is possible for a country to tailor the code list to their needs. Meaning, some countries might to decide not to include the Directives for the voluntary forms, others might not include something like `None`.
* It is already now possible to provide next to the predefined list under `Procedure Legal Basis (BT-01)` a textfield with the national legislation under `Procedure BT-01(d)`. It is already used in Germany and it is recommended to make use of this field.
* For `E1` and `E2` lots will be removed.
* For Defence, the contract modification notice will be a copy of 38. E6 will be independent on this. The reason is, that we will be more flexible with E6.
* As some countries have already implemented the voluntary forms, fields will itself not be removed but maybe only put optional. This will help those countries to update to the SDK 1.13 easier.
* To support the implementation of the voluntary forms, please provide feedback under this issue: https://code.europa.eu/eproc/eforms/crs/-/issues/40 

# Protocol

## Voluntary forms

### Usage of the Voluntary forms E1-E6

* from Jana Fafejtova CZ to everyone:  1:15 PM
  * Update for CZ - we are going to implement E1, and we are discussing about E6.
* from EC - MC to everyone:  1:16 PM
  * Many thanks Jana for this update. You can update the data already in the online version of the matrix.
* from Peter Kubovič to everyone:  1:17 PM
  * For Slovakia, we would like to use it (all of the possibilities) after we will solve our problem with supplier of eForms (we are in process of new public procurement).

### Group Framework Values

* from EC - MC to everyone:  1:20 PM
  * Is it okay to remove Group Framework values and group of lots and lots? It has no technical impact on the current eForms. Keeping them in would mean more implementation on your side (and ours)
* from DE - Rolf Kewitz BeschA to everyone:  1:18 PM
  * In Germany we are not using BG-557, BT-557. BT-157.
* from Marc Nosbusch to everyone:  1:19 PM
  * Ok for us to remove
* from Giampaolo Sellitto - ANAC IT to everyone:  1:19 PM
  * Ok      
* from BE - Michaël De Winne BOSA to everyone:  1:20 PM
  * OK for us
* from DE - Rolf Kewitz BeschA to everyone:  1:20 PM
  * O.k.
* from Alexandra Rodrigues PT to everyone:  1:20 PM
  * In PT we are not using BG-557, BT-557 and BT-157
* from Savina Kalanj AT to everyone:  1:20 PM
  * Yes, it's okay; relevant only to provide maximum value in CN (compliant with C-23/20)
* from Kenneth Skov Jensen to everyone:  1:20 PM
  * OK (DK)
* from Peter Kubovič to everyone:  1:20 PM
  * Yes
* from Jana Fafejtova CZ to everyone:  1:20 PM
  * OK for CZ
* from Andjelka Petreski NL to everyone:  1:20 PM
  * For the NL: it is ok for us (TenderNed)

### E6 and Contract modification

* from Jana Fafejtova CZ to everyone:  1:22 PM
  * Can E6 be used for defence?      
* from Peter to everyone:  1:23 PM
  * It is ok for TendrNed(NL) both E6 and form 41
* from Savina Kalanj AT to everyone:  1:23 PM
  * Would we need an amendment to add a form 41 then? When would this be ready? Also considering EDIP (not sure how you deal with ASAP/EDIRPA currently)
* from Jana Fafejtova CZ to everyone:  1:25 PM
  * So, there will be a special form for just for defence contract modification? Will it be included in SDK 1.13?
* from DE - Rolf Kewitz BeschA to everyone:  1:27 PM
  * O.k.
* from EC - MC to everyone:  1:29 PM
  * We will discuss with DEFIS on EDIP

### E4 for reporting

* from EC - MC to everyone:  1:33 PM
  * Use of E4 for reporting
* from Peter to everyone:  1:33 PM
  * I think the eForms forms should be pure and not be used for other purposes.
* from DE - Rolf Kewitz BeschA to everyone:  1:33 PM
  * Less rules means more freedom. That is fine (for E4).
* from Peter to everyone:  1:33 PM
  * It would it make more complex
* from EC - MC to everyone:  1:34 PM
  * Other ideas on voluntary forms
* from Giampaolo Sellitto - ANAC IT to everyone:  1:35 PM
  * Improve interoperability of eForms with data collection forms uses in other sectors
* from EC - MC to everyone:  1:39 PM
  * BE: Would like to not restrict voluntary forms for other only. It should include all Directives
* from OP - Enrico Campanella to everyone:  1:39 PM
  * the thresholds are set in the directives
* from Giampaolo Sellitto - ANAC IT to everyone:  1:48 PM
  * Same situation in Italy, we are trying to simplify eForms for below threshold
* from Giampaolo Sellitto - ANAC IT to everyone:  1:48 PM
  * we need a sort of "Core" information model for them to lower the administrative burden,
* from Andjelka Petreski NL to everyone:  1:49 PM
  * In NL it is not mandatory to publish below EU thresholds, so for us it has to be optional
* from Savina Kalanj AT to everyone:  1:50 PM
  * IE would likely still be bound by primary EU law below the threshold? (Also a sort of "other" law)
* from Giampaolo Sellitto - ANAC IT to everyone:  1:51 PM
  * We collect data for publication at national level and for transparency on direct awards and noncompetitive procedures, so we collect data from 0 value up
* from Kenneth Skov Jensen to everyone:  1:51 PM
  * I think we must assume, that national rules and EU rules, are not necessarily linked - only when transborder interest
* from Rune Kjørlaug NO-DFØ to everyone:  1:51 PM
  * We have cloned 4(E2), 16 (E3) and 29 (E4) into national forms - simplification is done by removing fields/rules (see line 65 and below https://github.com/anskaffelser/eforms-sdk-nor/blob/main/src/fields/national.yaml)
* from Peter to everyone:  1:52 PM
  * Yes, in the Netherlands situation we preferably have all fields optional and leave it to the user what to fill in. And we would publish it under the directive other.
* from OP - Karl Ferrand to everyone:  1:53 PM
  * It would be useful for EU Publications Office to have some sort of legal basis in each notice, even if another/national/exotic reason. We get resources to publish EU notices on TED because of the EU directives so in theory, why should we publish notices that are exclusively national? The costs are mostly marginal now that we have automatic validation, but we would have higher volumes to handle.
* from Duran Alp DE - Adesso SE to everyone:  1:54 PM
  * The Problem is you can't merge national legations of all the EU Countries to business rules. So, on EU side you have to put many fields optional, and the countries have to decide if its mandatory or not
* from Rune Kjørlaug NO-DFØ to everyone:  1:54 PM
  * The rest follows the standard eForms - as I understand it this is what MC is suggesting too?
* from Giampaolo Sellitto - ANAC IT to everyone:  1:55 PM
  * We have a national Code for below and above threshold procurement, together in Italy
* from EC - MC to everyone:  1:55 PM
  * Legal Basis add a code "None", okay?
* from Alexandra Rodrigues PT to everyone:  1:55 PM
  * In PT, even below thresholds, TED notice can be used, but it is not mandatory
* from OP - Karl Ferrand to everyone:  1:56 PM
  * No, "other" plus national. Actually, the best would be something like "2014/24" plus the national legislation for full clarity
* from Peter to everyone:  1:56 PM
  * Yes. MC, that would be correct.
* from Peter to everyone:  1:57 PM
  * Because of transparency
* from DE - Rolf Kewitz BeschA to everyone:  1:57 PM
  * In Germany, publications below the EU thresholds have NO reference to EU directives. They are related to national regulations! TED would only be an alternative to other publication forums. ("None" would not be a valid value for legal basis.)
* from Savina Kalanj AT to everyone:  1:57 PM
  * "none" reads like illegal procurement; this also seems difficult to understand for contracting authorities
* from Humann to everyone:  1:59 PM
  * Savina - I totally agree..
* from EC - MC to everyone:  1:59 PM
  * AT: None should not be used. Better to use "other" which could also include no legal basis
* from OP - Karl Ferrand to everyone:  1:59 PM
  * this is a summary of the "legal bases" we currently publish on TED: https://ted.europa.eu/en/simap/european-public-procurement#publish
* from OP - Karl Ferrand to everyone:  2:01 PM
  * we haven't bothered to have all possibilities in the code list for BT-01 and use "Other" for the other cases, like a "none of the above"
* from EC - MC to everyone:  2:01 PM
  * NL: Is not sure about using only "other". Will reflect on this.
* from OP - Karl Ferrand to everyone:  2:01 PM
  * "Other" then explain what "none" means for you.
* from Rune Kjørlaug NO-DFØ to everyone:  2:01 PM
  * New code "thin"
* from DE - Rolf Kewitz BeschA to everyone:  2:02 PM
  * If it is "Other". Where is the place to explain what the legal basis is?
* from Shane Forde - IE to everyone:  2:02 PM
  * IE- Happy to take it back and see if we have a code list suggestion.
* from OP - Enrico Campanella to everyone:  2:03 PM
  * BT-01(d)-Procedure = free text to express any legal basis that doesn't have a code or is not an EU Directive
* from OP - Karl Ferrand to everyone:  2:03 PM
  * Germany already does this
* from DE - Rolf Kewitz BeschA to everyone:  2:03 PM
  * Additional text filed would be fine.
* from OP - Karl Ferrand to everyone:  2:03 PM
  * 2.1.4.General information Legal basis: Directive 2014/24/EU vgv -
* from OP - Karl Ferrand to everyone:  2:03 PM
  * Random example: https://ted.europa.eu/en/notice/-/detail/370814-2024
* from Duran Alp DE - Adesso SE to everyone:  2:04 PM
  * In Germany we already used this
* from Duran Alp DE - Adesso SE to everyone:  2:06 PM
  * In Germany we provide a Code list, for these codes. Yes, its mapped
* from Duran Alp DE - Adesso SE to everyone:  2:06 PM
  * Our Central eSender does the work
* from EC - MC to everyone:  2:10 PM
  * BT-01 we will provide the current list of Directives and also “other”. Maybe and additional will be added to show that the buyer published for transparency reasons. Okay?
* from BE - Michaël De Winne BOSA to everyone:  2:10 PM
  * Works for us
* from Kenneth Skov Jensen to everyone:  2:11 PM
  * Fine by us (DK)
* from OP - Karl Ferrand to everyone:  2:11 PM
  * Transparency is a subset of Other
* from Jana Fafejtova CZ to everyone:  2:12 PM
  * "Other" is ok form us, we can map it to our public procurement law.
* from EC - MC to everyone:  2:13 PM
  * Fields in the voluntary forms
* from Rune Kjørlaug NO-DFØ to everyone:  2:13 PM
  * Fine for Norway to - we already use another below threshold
* from EC - MC to everyone:  2:14 PM
  * BE: Would like that no fields will be removed, because then their implementation would not be compatible anymore.
* from OP - Karl Ferrand to everyone:  2:18 PM
  * What the minimum rules that need to be kept ensuring basic coherence within the notice?
* from Giampaolo Sellitto - ANAC IT to everyone:  2:18 PM
  * But could we consider that eForms for below threshold procurement as another eForm (optional) else putting some fields as optional in E! they will become optional for all
* from OP - Karl Ferrand to everyone:  2:19 PM
  * Did BE add some fields to the forms based on 2014/14 form, for example, to include the handful of fields specific to concessions or defence?
* from BE - Roel Arys to everyone:  2:20 PM
  * I don't think so Karl, but we'll have a chat with Mira to be absolutely sure.
* from BE - Roel Arys to everyone:  2:21 PM
  * I do think we have made some existing optional fields mandatory (strangely :) ) ,but that shouldn't be a problem
* from Peter to everyone:  2:22 PM
  * Yes, that is also a situation in the NL. that buyers below threshold still publish on TED. if there are in NL few suppliers
* from OP - Karl Ferrand to everyone:  2:23 PM
  * So, in general, can we implement the below threshold forms based exclusively on 2024/14? It wouldn't be possible to include fields that exist only for the other directives.
* from BE - Michaël De Winne BOSA to everyone:  2:24 PM
  * We offer e.g.E3 for D25 as well
* from OP - Enrico Campanella to everyone:  2:24 PM
  * so you have different rules for E3 (dir24) and E3 (dir25). we would have to add many rules or have the same rules as for above threshold forms
* from EC - MC to everyone:  2:28 PM
  * We have to look into BT-10 and BT-610 for the voluntary forms because BE also uses it for Dir25
* from OP - Enrico Campanella to everyone:  2:31 PM
  * BT_105 and BT-106 (procedure type + Accelerated procedure) available in E3 but not in 19
* from Peter to everyone:  2:35 PM
  * I agree to MC,.
* from Jana Fafejtova CZ to everyone:  2:38 PM
  * We currently use eForms 1 - 40 also for below-the-threshold tenders, but there is no obligation to publish them in TED, so these forms are published only on national level. That's why we are going to implement only E1 for preliminary market consultations.
* from BE - Michaël De Winne BOSA to everyone:  2:39 PM
  * About the fear of seeing too many Ex forms instead of the official ones: in BE, if your estimated value is above EU threshold
   --> notice has to contain at least X, Y, Z
    --> only the official forms (e.g.) 16 cover this need
    Meaning, if a buyer were to choose E3 in that case, simply because it is simpler, that option would be illegal.
* from Peter to everyone:  2:40 PM
  * Can we have the spreadsheet about the voluntary forms?
* from DE - Rolf Kewitz BeschA to everyone:  2:40 PM
  * Could you please provide the link to place in Gitlab for placing comments you have asked for?

## Other issues 

### Issue 51 Multiple lots

* from EC - MC to everyone:  2:43 PM
  * Issue for providing feedback on Voluntary forms https://code.europa.eu/eproc/eforms/crs/-/issues/40
* from FR Audrey AGENJO DAJ/MEFSIN to everyone:  2:44 PM
  * Thank you very much Karl
* from EC - MC to everyone:  2:44 PM
  * https://code.europa.eu/eproc/eforms/crs/-/issues/12
* from FR Audrey AGENJO DAJ/MEFSIN to everyone:  2:45 PM
  * So, you recommend multiple notices? 

### Review

* from DE - Rolf Kewitz BeschA to everyone:  2:47 PM
  * We are not using BG-716 in Germany.
* from EC - MC to everyone:  2:49 PM
  * Other issues to discuss?
* from FR Audrey AGENJO DAJ/MEFSIN to everyone:  2:50 PM
  * Has the EU provided for an eSender to take over a procedure started on eNotices2 and complete it itself in its buyer profile? If blocked, can you unblock?
* from FR Audrey AGENJO DAJ/MEFSIN to everyone:  2:52 PM
  * Ok, thank you!
* from EC - MC to everyone:  2:52 PM
  * OP: It is possible to take over procedures from eNotices2 to the national platform
* from DE - Rolf Kewitz BeschA to everyone:  2:56 PM
  * When will there be a follow-up date for E1-E6?
* from Andjelka Petreski NL to everyone:  2:56 PM
  * Thanks
* from Giampaolo Sellitto - ANAC IT to everyone:  2:57 PM
  * We have a pre-litigation procedure in Italy, managed by ANAC and it leads to a non-binding opinion that is binding in the case it is asked both by CA and EO or the Administrative Court
* from FR Audrey AGENJO DAJ/MEFSIN to everyone:  2:58 PM
  * Last question regarding eForms translations. We have some questions and comments that arised while reading the table :
   \-  We are unable to understand the difference between BT-3201 and BT-3202.
    \-  Ditto for fields BT-6110 and BT-6140.
    \-  We need clarification on the expected information that should be filled in BT-70.
    \-  OPT and OPP fields are included in this table, but we are unable to identify what information is expected in these fields, or where in the notice they apply.
    \-  Ditto for GR fields. 
* from Savina Kalanj AT to everyone:  2:59 PM
  * These systems will be different between MS, so we may not have to reach for same interpretations across MS. So for NL, it could be considered to fill out "other", but also to choose "1st instance", depending on what allows for better data analysis for you?
* from Andjelka Petreski NL to everyone:  2:59 PM
  * not first instance as it is non judicial in the case of complaints. but other will do
* from Giampaolo Sellitto - ANAC IT to everyone:  3:00 PM
  * We have first and second instance administrative eCourt actually
* from Savina Kalanj AT to everyone:  3:00 PM
  * Czechia has two instances that are not judicial and two that are judicial, as an example :) not sure how they would fill this out but filling out "other" twice would be confusing for them
* from Andjelka Petreski NL to everyone:  3:02 PM
  * you are in for a treat with this one ;-)
* from OP - Karl Ferrand to everyone:  3:02 PM
  * To check demand: how many MS are waiting/wanting to have a Review section in Result notices?
* from OP - Karl Ferrand to everyone:  3:02 PM
  * Just to see if this is absolutely needed by October...
* from BE - Michaël De Winne BOSA to everyone:  3:03 PM
  * We're of course not against it, but we haven't received any feedback that it's needed urgently either.
* from Andjelka Petreski NL to everyone:  3:03 PM
  * We are not in a hurry with this one, but we have to check it internally before I come with a definite wishlist for this one
* from EC - MC to everyone:  3:04 PM
  * Who is needing the section on Review urgently?

### Priorities for SDK 1.13

* from EC - MC to everyone:  3:06 PM
  * What priorities would you have for SDK 1.13?
* from Savina Kalanj AT to everyone:  3:07 PM
  * Voluntary forms most urgent; then IPI, then review, then EED
* from DE - Rolf Kewitz BeschA to everyone:  3:07 PM
  * Germany: Early provision of E2-E4 is urgently needed!

## Closing

### Farewell

* from OP - Karl Ferrand to everyone:  3:09 PM
  * Might be sunnier in Brussels by September
* from PT - Sandra Mascarenhas to everyone:  3:10 PM
  * thank you. have a nice weekend
* from Jason Grech to everyone:  3:10 PM
  * Bye
* from FR Audrey AGENJO DAJ/MEFSIN to everyone:  3:10 PM
  * Thank you bye !
* from BE - Michaël De Winne BOSA to everyone:  3:10 PM
  * Thx, bye!
* from Marc Nosbusch to everyone:  3:10 PM
  * Bye
* from Carmen Collantes to everyone:  3:10 PM
  * thank you!
* from Giampaolo Sellitto - ANAC IT to everyone:  3:10 PM
  * Bye
* from Humann to everyone:  3:10 PM
  * Nice weekend Paul
* from BE - Roel Arys to everyone:  3:10 PM
  * Thank you.. Have a great weekend
* from Jana Fafejtova CZ to everyone:  3:10 PM
  * Bye
* from OP - Karl Ferrand to everyone:  3:10 PM
  * Bye, thanks!
* from SI Aleksandra Jesenovec Ljubojević to everyone:  3:10 PM
  * Bye
* from Cátia Miguel PT to everyone:  3:10 PM
  * Thank you
