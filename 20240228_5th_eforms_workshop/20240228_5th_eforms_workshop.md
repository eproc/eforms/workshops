---
title: 5th eForms Workshop
author: DG GROW.G4
date: 28 February 2024
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{noto}
---

# Agenda 

The 5th eForms Workshop takes place on the **28 February 2024**, **from 9:00 to 11:00**.

* 09:00 – 09:10: Introduction
* 09:10 – 09:30: New way of working
* 09:30 – 10:00: Guides
* 10:00 – 10:50: AOB
* 10:50 – 11:00: Feedback and next steps

# Introduction

## Etiquette for online meetings

1. During the presentation, keep your **mic off** and **video off**.
2. Use the **chat** to ask your questions.
3. The session will **not be recorded**.
4. Don't be shy to **participate actively** throughout the session.
5. Please **raise your hand** if you wish to take the floor.

## Organisational aspects

* As it is not always possible that all can participate, please try to get a substitute for you.
* Is the agenda okay for today?

# New way of working

As of this workshop, we will use the tool https://code.europa.eu/eproc/eforms. The aim is to provide transparency to the future development of eForms.

Currently, there are three groups:

* **Change Requests**: This group is used for all issues. If there is something you would like to discuss, please use https://code.europa.eu/eproc/eforms/crs/-/issues. Only policy or business requirements should be encoded here. Technical issues on should be reported to <OP-TED-HELPDESK@publications.europa.eu>. We will also use issues, when new guides are created, to provide a possibility for providing feedback.
* **Docs**: Here we will provide all documentation and guides. The current guides you will find here https://code.europa.eu/eproc/eforms/docs/-/tree/main/guides.
* **Workshops**: Here we will provide all information on the different workshops.

While the new way of working will help to make the development transparent, it is always possible to use the existing channels for communication. They will complement each other.

If you have ideas on how to improve it, we would welcome your feedback!

# Guides

We would like to discuss the following guides during this session:

| Name         | Title                                                    | Description                                                                           | Files                   |
|--------------|----------------------------------------------------------|---------------------------------------------------------------------------------------|-------------------------|
| gde_ 001_fsr | eForms Guide on the Foreign Subsidy Regulation           | The guide shows on how the Foreign Subsidy Regulation is  applied to eForms.          | [Link](https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_001_fsr.md), [PDF](https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_001_fsr.pdf) |
| gde_ 002_ipi | eForms Guide on the International Procurement Instrument | The guide shows on how the International Procurement Instrument is applied to eForms. | [Link](https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_002_ipi.md)  [PDF](https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_002_ipi.pdf) |

Feedback on the guides can be made here:

* [Issue 8](https://code.europa.eu/eproc/eforms/crs/-/issues/8) for FSR
* [Issue 10](https://code.europa.eu/eproc/eforms/crs/-/issues/10) for IPI

The idea of discussing these two guidelines is to have a shared understanding on how FSR and IPI work. With your feedback, we will technically implement them into a future SDK so that they can be made available as of June 2024.

# AOB

Issues that we would like to discuss:

* https://code.europa.eu/eproc/eforms/crs/-/issues/4 on "Main activity"—code list (BT-10 & BT-610)
* https://code.europa.eu/eproc/eforms/crs/-/issues/3 on "Buyer Legal Type"—code list (BT-11)

Are there other issues that you would like to discuss?

# Closing

## Next steps

* We will provide some minutes on this workshop also on code.europa.eu.
* We are working on the guideline of the EED.
* We would like to work on the voluntary forms of eForms E1 to E6. If you have started to do this in your country, could you please send us to <grow-eprocurement@ec.europa.eu> your tailored Annex so that we can start to compare it. We would like to start to discuss this in the next eForms workshop on the 18 April.
* Please feel free to provide comments here https://code.europa.eu/eproc/eforms/crs/-/issues.

## Upcoming events

* 19/20 March 2024: Innovation Procurement Conference. This conference is part of the Belgian Presidency of the Council of the European Union and will be organised during the Research & Innovation Week. [Link](https://www.ewi-vlaanderen.be/en/events/innovation-procurement-conference)
* 18 April 2024: Next eForms workshop meeting in Tirana, Albania (hybrid).
* 29/30 April 2024: The Publications Office is organizing the event "Public Procurement Data Superpowers: Transparency in public procurement – Harnessing artificial intelligence". Pre-registration will remain available until 1 March 2024. As space is limited, [book your place now](https://op.europa.eu/en/web/public-procurement-data-superpowers/registration)!

## Feedback

How did you like the new way of doing the eForms workshop?