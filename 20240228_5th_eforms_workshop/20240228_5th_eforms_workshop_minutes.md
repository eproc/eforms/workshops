---
title: 5th eForms Workshop Minutes
author: DG GROW.G4
date: 28 February 2024
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
 \usepackage{noto}
---

# Photo

![Photo of participants](20240228_5th_eforms_workshop_photo.png)

# Upcoming events

* 19/20 March 2024: Innovation Procurement Conference. This conference is part of the Belgian Presidency of the Council of the European Union and will be organised during the Research & Innovation Week. [Link](https://www.ewi-vlaanderen.be/en/events/innovation-procurement-conference)
* 18 April 2024: Next eForms workshop meeting in Tirana, Albania (hybrid).
* 29/30 April 2024: The Publications Office is organizing the event "Public Procurement Data Superpowers: Transparency in public procurement – Harnessing artificial intelligence". Pre-registration will remain available until 1 March 2024. As space is limited, [book your place now](https://op.europa.eu/en/web/public-procurement-data-superpowers/registration)!

# Action items

## Member States

1. **FSR and IPI**: Please review the _IPI and FSR Guides_ by the **15 March** and include your feedback by using [Issue 8](https://code.europa.eu/eproc/eforms/crs/-/issues/8) for _FSR_ and [Issue 10](https://code.europa.eu/eproc/eforms/crs/-/issues/10) for _IPI_.
2. **Main activity - code list**: Please provide further feedback on, if we should move towards COFOG for _Main Activity_ through [Issue 4](https://code.europa.eu/eproc/eforms/crs/-/issues/4). We would like to close the discussion at the _6th eForms workshop_ on the 18 April.
3. **Main activity - combine fields**: Should the fields _Activity Entity_ (BT-610) and _Activity Buyer_ (BT-12) be combined? [Issue 1](https://code.europa.eu/eproc/eforms/crs/-/issues/1).  We would like to close the discussion at the _6th eForms workshop_ on the 18 April.
4. **Buyer legal type**:: Please provide a list of a buyer legal type that would make sense in your country: [Issue 3](https://code.europa.eu/eproc/eforms/crs/-/issues/3). We would like to close the discussion at the _6th eForms workshop_ on the 18 April.
5. **General**: Please provide input on the topics we discussed by logging it as a change request via this link https://code.europa.eu/eproc/eforms/crs/-/issues
6. **General**: Let us know which issue should be discussed in the upcoming eForms workshop

**Note**: When you log into GitLab the first time with your EU-Login, you cannot create issues or comments immediately. Instead, you might receive first an eMail from the owner of GitLab to write him back. Only then you will be granted write access. If the issue persists, please contact us by <grow-eprocurement@ec.europa.eu>.

## European Commission
7. **FSR and IPI**: Include information on which fields are repeatable in the guides for FSR and IPI [Issue 14](https://code.europa.eu/eproc/eforms/crs/-/issues/14)
8. **IPI**: To clarify at what level is each IPI business group info provided. According to the Annex it is at tender level [Issue 15](https://code.europa.eu/eproc/eforms/crs/-/issues/15).
9. **IPI**: To clarify, if IPI measures are also applicable to Defence procurement [Issue 19](https://code.europa.eu/eproc/eforms/crs/-/issues/19).
10. **FSR**: To copy the field on whether FSR is applicable (BT-681) from the competition notice into the contract award notice, as the two notices are not technically connected [Issue 21](https://code.europa.eu/eproc/eforms/crs/-/issues/21).
11. **FSR and IPI**: Finalise the IPI and FSR Guides based on the comments received. For IPI [Issue16](https://code.europa.eu/eproc/eforms/crs/-/issues/16) and for FSR [Issue 17](https://code.europa.eu/eproc/eforms/crs/-/issues/17).
12. **FSR and IPI**: Change the optional fields of _Foreign Subsidies Regulation_ (BT-681) and _International Procurement Instrument Regulation_ (BT-684) from _optional_ to _existing mandatory_ [Issue 18](https://code.europa.eu/eproc/eforms/crs/-/issues/18).
13. **3rd Amendment**: We will provide an Excel sheet on GitLab, where we will show the changes that are foreseen for the 3rd eForms amendment in [Issue 20](https://code.europa.eu/eproc/eforms/crs/-/issues/20).
14. **Translations**: Reach out to the Member States to collect contact points for the translations for example on business rules. We will ask this in our next eMail to the eForms group.
15. **6th eForms workshop**: Send further information on the upcoming 6th eForms workshop which will be held on the 18 April in Tirana (hybrid).
16. **Evaluation questionnaire**: Share the evaluation questionnaire with the Member States.

# Protocol

## Guides

### IPI

* from EC - MC to Everyone: 9:15 AM
  * Topic: Guides
* from EC - MC to Everyone: 9:17 AM
  * gde-002-IPI
* from Laurent Schoonjans - DG GROW to Everyone: 9:19 AM
  * https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_002_ipi.md
* from EC - MC to Everyone: 9:28 AM
  * **How do you like the structure of the gide on IPI?**
* from Savina Kalanj AT to Everyone: 9:29 AM
  * The structure looks very useful! Maybe adding information on repeatable fields would be useful?
* from Renzo Kottmann CoSIT to Everyone: 9:29 AM
  * business rules are very valuable
* from FI Petteri Pohto to Everyone: 9:29 AM
  * The structure seems logical and good
* from EC - MC to Everyone: 9:29 AM
  * Good point! We can add information if fields are repeatable.
* from BE - Michaël De Winne BOSA to Everyone: 9:30 AM
  * A big step forward to have a clear overview on upcoming measures
* from EC - MC to Everyone: 9:30 AM
  * Do you have any questions on IPI itself?
* from Dana Coleova SK to Everyone: 9:30 AM
  * The structure seems ok, easy to follow
* from EC - MC to Everyone: 9:31 AM
  * Question to say more about on how to apply it.
* from Renzo Kottmann CoSIT to Everyone: 9:31 AM
  * Am I right if I interpret "opt-in" as existing mandatory?
* from FI Petteri Pohto to Everyone: 9:34 AM
  * Could you provide the relevant codelists on this guide or as an attachment?
* from OP - Karl Ferrand to Everyone: 9:34 AM
  * "existing mandatory" is implemented as optional, i.e. no rules. The default for the BT-684 flag is simply "empty" rather than "no".
* from OP - Karl Ferrand to Everyone: 9:39 AM
  * To clarify: at what level is each IPI business group info provided? It's currently (in the annex) linking one or more IPI business group for each tender
* from EC - MC to Everyone: 9:45 AM
  * We will check if it is on procedure or on lot level. So likely it will be on procedure/tender level. It will be checked by TRADE.

### FSR

* from EC - MC to Everyone: 9:47 AM
  * Discussion on FSR
* from Laurent Schoonjans - DG GROW to Everyone: 9:47 AM
  * https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_001_fsr.md
* from Renzo Kottmann CoSIT to Everyone: 9:51 AM
  * Why do you suggest BT-300? Would it be possible to have a special field for this?
* from Savina Kalanj AT to Everyone: 9:51 AM
  * How does the business rule work between CN (681) and CAN (682)?
* from PPA-BG to Everyone: 9:55 AM
  * There are FSR Declaration / Notice available in English and French. Do you envisage to provide such also on other languages?
* from OP - Karl Ferrand to Everyone: 9:56 AM
  * BT-300 is a free text field, so it's only a suggestion for buyers to use it until the new FSR fields are added
* from PPA-BG to Everyone: 10:00 AM
  * Are there fields that "disappear" in Annex2 in comparison to previous versions (Regulation itself and the Annex1)?
* from Alexandra Rodrigues PT to Everyone: 10:00 AM
  * In Portugal, we didn't implement BT-300
* from George Vernardos to Everyone: 10:00 AM
  * Actually, the "Warnings" were mostly ignored...
* from EC - MC to Everyone: 10:04 AM
  * We would copy the field from CN to BT-681 into the CAN to apply the business rules.
* from OP - Enrico Campanella to Everyone: 10:04 AM
  * How to cater for exceptions and member state specific implementations of the directives?
* from Pierre HOULLEMARE to Everyone: 10:08 AM
  * Thank you for this btw
* from Andjelka Petreski NL to Everyone: 10:13 AM
  * Yes, that would be useful - showing changes
* from Zsófia Dudás - DG GROW to Everyone: 10:13 AM
  * https://single-market-economy.ec.europa.eu/single-market/public-procurement/digital-procurement/eforms_en

## AOB

### Issue 4 on "Main activity" - codelist (BT-10 & BT-610)

* from EC - MC to Everyone: 10:15 AM
  * AOB
* from Meiszterics Gábor - KH to Everyone: 10:19 AM
  * Can you please share the link of this document by EUROSTAT?
* from EC - MC to Everyone: 10:19 AM
  * https://ec.europa.eu/eurostat/documents/3859598/10142242/KS-GQ-19-010-EN-N.pdf/ed64a194-81db-112b-074b-b7a9eb946c32?t=1569418084000
* from Meiszterics Gábor - KH to Everyone: 10:19 AM
  * Thanks
* from BE - Michaël De Winne BOSA to Everyone: 10:22 AM
  * But this looks like a more detailed list of BT-11 buyer legal type. Would you really use this to replace BT-10 and BT-610?
* from Savina Kalanj AT to Everyone: 10:22 AM
  * So you would have a long list of all the codes within each category? Because the codelist right now already replicates these codes as far as I can tell
* from EC - MC to Everyone: 10:24 AM
  * We would provide the code list for the two levels, so the buyer could choose one of the 10 or select a lower category if they wish to do so.
* from OP - Karl Ferrand to Everyone: 10:25 AM
  * And also merge BT-10 and BT-610, or do we still need to distinguish activity of authority in a different BT to activity of entity?
* from Renzo Kottmann CoSIT to Everyone: 10:25 AM
  * When do you consider using this in the future? Until when do you need feedback?
* from BE - Michaël De Winne BOSA to EC - MC (privately): 10:27 AM
  * Same question as Karl (I think): would you merge BT-10 and BT-610? If not, how will we know which code is allowed in which BT?
* from BE - Michaël De Winne BOSA to Everyone: 10:28 AM
  * Same question as Karl (I think): would you merge BT-10 and BT-610? If not, how will we know which code is allowed in which BT?
* from EC - MC to Everyone: 10:28 AM
  * Our proposal would be to use COFOG in the future. What is your current impression? (Not binding)
* from OP - Karl Ferrand to Everyone: 10:28 AM
  * Some entries in the current main-activity codelist refer to COFOG: https://op.europa.eu/en/web/eu-vocabularies/concept-scheme/-/resource?uri=http://publications.europa.eu/resource/authority/main-activity
* from Renzo Kottmann CoSIT to Everyone: 10:29 AM
  * Looks good to me.
* from EC - MC to Everyone: 10:30 AM
  * We would also merge BT-10 and BT-610.
* from Savina Kalanj AT to Everyone: 10:30 AM
  * Looks good, as long as the general categories remain available (and not only the full detailed lists)
* from FI Petteri Pohto to Everyone: 10:30 AM
  * Looks fine
* from BE - Michaël De Winne BOSA to Everyone: 10:30 AM
  * And so any code would work on any legal basis?
* from Maarika Tork EE to Everyone: 10:30 AM
  * Merging is a good idea.
* from BE - Michaël De Winne BOSA to Everyone: 10:31 AM
  * No, but BT-610 and BT-10 are linked to different legal bases
* from BE - Michaël De Winne BOSA to Everyone: 10:31 AM
  * In the sense that they do not appear in the same notice types
* from BE - Michaël De Winne BOSA to Everyone: 10:32 AM
  * Well, merging everything into 1 BT used throughout each notice subtype is of course simple, but that is our question :-)
* from Maarika Tork EE to Everyone: 10:32 AM
  * They might if it is a joint procurement
* from EC - MC to Everyone: 10:34 AM
  * Please provide your feedback in https://code.europa.eu/eproc/eforms/crs/-/issues/4. We would discuss and close this in April at the next eForms workshop.
* from EC - MC to Everyone: 10:35 AM
  * On the merging there is already issue, https://code.europa.eu/eproc/eforms/crs/-/issues/1 please provide your feedback on this one here.

### Issue 3 on "Buyer Legal Type" - codelist (BT-11) 

* from EC - MC to Everyone: 10:37 AM
  * Buyer legal type BT-11 https://code.europa.eu/eproc/eforms/crs/-/issues/3
* from BE - Michaël De Winne BOSA to Everyone: 10:38 AM
  * We often get a remark about this codelist too from buyers: "we can't find something that suits us".
* from Renzo Kottmann CoSIT to Everyone: 10:40 AM
  * Main reason was that we needed to accommodate for the different federal levels
* from Savina Kalanj AT to Everyone: 10:41 AM
  * Shouldn't this list reflect Art. 2(1) to (4) of Directive 2014/24/EU (and see respectively other Directives)?
* from EC - MC to Everyone: 10:41 AM
  * Please provide a list of a buyer legal type that would make sense in your country: https://code.europa.eu/eproc/eforms/crs/-/issues/3
* from Alex Mohora to Everyone: 10:42 AM
  * We have the same situation in Romania - there is a list at national level, and we map it with the EU codelist buyer legal type
* from Savina Kalanj AT to Everyone: 10:42 AM
  * Art. 2(1) 1 to 4, more precisely, sorry
* from Renzo Kottmann CoSIT to Everyone: 10:45 AM
  * Although it is a technical document it gives an example on how we tailor buyer legal type and map it to EU code list: https://projekte.kosit.org/eforms/eforms-de-codelist/-/blob/main/doc/tailoring.md
* from EC - MC to Everyone: 10:46 AM
  * Could you provide the input until April for the next eForms workshop?
* from Pierre HOULLEMARE to Everyone: 10:47 AM
  * I can see why you want to continue using codelists and avoid the pitfalls of adding a text field. On the other hand, don't you think that the consequence of mapping is not to reflect the exact reality on the terrain and therefore to end up with data that is not representative at EU level?
* from Pierre HOULLEMARE to Everyone: 10:48 AM
  * Understood
* from Duran Alp DE - Adesso SE to Everyone: 10:48 AM
  * How about to technically support national codelist with back reference, like a attribute on xml element with the link to the national list?
* from Renzo Kottmann CoSIT to Everyone: 10:49 AM
  * Not a good idea, because it would incredibly bloat the xml
* from PPA-BG to Everyone: 10:50 AM
  * Is it possible to have an additional (different) value in the codelist BT-105 - for the exceptions from the scope of the Directives (as in fact, they have not been awarded as a result of a procurement procedure, so negotiation without prior publication is not the best choice, but the only one at the moment). This should be in addition to the choice made in BT-01 ("Other")
* from Renzo Kottmann CoSIT to Everyone: 10:50 AM
  * An approach could be to collect all needed code from all MS
* from Renzo Kottmann CoSIT to Everyone: 10:51 AM
  * E.g. if you would take the German Codes, we could stop the tailoring (and save effort and money)
* from Renzo Kottmann CoSIT to Everyone: 10:51 AM
  * and save

## Closing

### Upcoming events

* from EC - MC to Everyone: 10:53 AM
  * Closing
* from OP - Karl Ferrand to Everyone: 10:54 AM
  * Next eSender workshop on 20 March, then 3 more this year: https://op.europa.eu/en/web/ted-eforms/
* from BE - Michaël De Winne BOSA to Everyone: 10:56 AM
  * Résidence palace, but I haven't been there myself either :-)

### Feedback

* from Renzo Kottmann CoSIT to Everyone: 10:57 AM
  * Love it
* from Duran Alp DE - Adesso SE to Everyone: 10:57 AM
  * Its more transparent, thank you very much
* from Maarika Tork EE to Everyone: 10:57 AM
  * Very nice, I liked it a lot.
* from Savina Kalanj AT to Everyone: 10:57 AM
  * Works very well!
* from Dana Coleova SK to Everyone: 10:58 AM
  * Works well, thank you
* from DK Simon BUSCH to Everyone: 10:59 AM
  * Please do
* from BE - Michaël De Winne BOSA to Everyone: 11:01 AM
  * A reasonable extension of the validity of current SDK versions would definitely be welcome!
* from Maarika Tork EE to Everyone: 11:01 AM
  * Agree
* from PPA-BG to Everyone: 11:01 AM
  * Agree!
* from Renzo Kottmann CoSIT to Everyone: 11:02 AM
  * Agree

### Farewell

* from Lora Van Looveren - DG GROW to EC - MC (privately): 11:03 AM
  * Thanks!
* from Pierre HOULLEMARE to Everyone: 11:03 AM
  * Thank you for this workshop. Bye.
* from FI Petteri Pohto to Everyone: 11:03 AM
  * Thanks!
* from PT- Sandra Mascarenhas to Everyone: 11:03 AM
  * Bye bye, thanks
* from Alexandra Rodrigues PT to Everyone: 11:03 AM
  * Thank you. Bye
* from Renzo Kottmann CoSIT to Everyone: 11:03 AM
  * Bye thx a lot
* from OP - Karl Ferrand to Everyone: 11:04 AM
  * Thanks!
* from Jana Fafejtova CZ to Everyone: 11:04 AM
  * Thank you, bye
* from BE - Mira Ratajczak to Everyone: 11:04 AM
  * Thanks bye
* from LU - Marc Nosbusch to Everyone: 11:04 AM
  * Thanks. Bye.
