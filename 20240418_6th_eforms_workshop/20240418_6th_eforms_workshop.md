---
title: 6th eForms Workshop
date: 16 April 2024
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
---

# Agenda 

The 6th eForms Workshop takes place on the **18 April 2024**, **from 13:00 to 15:30**.

* 13:00 – 13:10: Introduction
* 13:10 – 14:30: Issues follow up 
* 14:30 – 15:00: Feedback on the eForms survey
* 15:00 – 15:15: Update from OP 
* 15:15 – 15:20: AOB
* 15:20 – 15:30: Feedback and next steps

# Introduction

## Etiquette for all participants

1. During the presentation, keep your **mic off** and **video off**.
2. Use the **chat** to ask your questions.
3. The session will **not be recorded**.
4. Don't be shy to **participate actively** throughout the session.
5. Please **raise your hand** if you wish to take the floor.

## Organisational aspects

* When you log into GitLab the first time with your EU-Login, you cannot create issues or comments immediately. Instead, you might receive first an eMail from the owner of GitLab to write him back. Only then you will be granted write access. If the issue persists, please contact us by <grow-eprocurement@ec.europa.eu>.
* As it is not always possible that all can participate, please try to get a substitute for you.
* Is the agenda okay for today?

# Issues follow up

We would like to put the issues follow up into three buckets. All issues that we would like to discuss are marked with the label `event::6th_workshop`. We might mark more issues with this label before the workshop:
1. Follow up on FSR
2. Go through closed issues
3. Go through open issues

## Follow up on FSR

We need to close the discussions about the FSR, so that the Publications Office can implement it in SDK 1.12.

First and foremost, we have now worked on the code list with its definitions and would be happy if you could have a look at it [here](https://code.europa.eu/eproc/eforms/docs/-/blob/main/codelists/cdl_001_fsr.md).

We have adjusted accordingly the list in the guide itself, which you can find [here](https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_001_fsr.md).

Please provide your comments in [this issue](https://code.europa.eu/eproc/eforms/crs/-/issues/39).

All issues around FSR you will be shown when clicking on the following [link](https://code.europa.eu/eproc/eforms/crs/-/issues/?sort=updated_desc&state=all&label_name%5B%5D=topic%3A%3Afsr&first_page_size=20).

## Go through closed issues

The list of closed issues we would like to quickly go through on Thursday is [here](https://code.europa.eu/eproc/eforms/crs/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=event%3A%3A6th_workshop&first_page_size=20).

## Go through open issues

### eForms Guide on Exclusion Grounds and Selection Criteria

The eForms Guide on Exclusion Grounds and Selection Criteria is available. Please be aware that we needed to make some textual changes (indicated in the guide). 

The Guide is available [here](https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_003_esc.md).
The PDF version is available [here](https://code.europa.eu/eproc/eforms/docs/-/blob/main/guides/gde_003_esc.pdf).
Please provide your comments [here](https://code.europa.eu/eproc/eforms/crs/-/issues/41).

### Other open issues

* The list of open issues, which we would like to go through on Thursday is [here](https://code.europa.eu/eproc/eforms/crs/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=event%3A%3A6th_workshop&first_page_size=20).

* We provide now an Excel that includes changes that we will address in the 3rd Amendment. You can find it [here](https://code.europa.eu/eproc/eforms/crs/-/issues/20).

* We need to provide an updated eForms Governance document. You can find the corresponding issue [here](https://code.europa.eu/eproc/eforms/crs/-/issues/42).:w
* 

* As we will soon have to work also on the specification on the voluntary forms, for the countries who are already using eForms for below the EU Thresholds, please provide us input [here](https://code.europa.eu/eproc/eforms/crs/-/issues/40).

# Update from OP

## Roadmap for this year

* June release — SDK 1.12:
  * FSR
  * Exclusion Grounds and Selection Criteria (ESC)
  * Text corrections
* October release — SDK 1.13:
  * IPI
  * EED
  * Voluntary forms
* Maintenance of the SDK

## Other

* Longer support version of SDKs
* Translations

# AOB

Are there other issues that you would like to discuss?

# Closing

## Next steps

* We will provide some minutes on this workshop also on code.europa.eu.
* We will work on guides that are necessary for the eForms Amendment (EU) 2023/2884 like EED.
* Please feel free to provide comments [here](https://code.europa.eu/eproc/eforms/crs/-/issues).

## Upcoming events

* 29/30 April 2024: The Publications Office is organizing the event "Public Procurement Data Superpowers: Transparency in public procurement – Harnessing artificial intelligence".
* 24 September 2024: PPDS Day 2024 in Brussels.

## Feedback

How did you like this workshop?