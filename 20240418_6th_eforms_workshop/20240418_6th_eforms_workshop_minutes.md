---
title: 6th eForms Workshop Minutes
author: DG GROW.G4
date: 18 April 2024
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
 \usepackage{noto}
 \usepackage[left]{lineno}
 \linenumbers
---

# Photo

![Photo of participants participating virtually](20240418_6th_eforms_workshop_photo_1.jpg)

![Photo of participants in the room](20240418_6th_eforms_workshop_photo_2.jpg)

# Protocol

## Foreign Subsidy Regulation Code List

* OP: We noticed that codes 4 and 5 are duplicates in the code list, could you please correct this?
* EC: Indeed, there has been an error, we will update the code list.
* FR: The guides are important, however when it comes to the code list it would be important for us to have more detail. In our view, the code lists are not detailed enough, which makes it more difficult for us to understand. 
* EC: For the FSR, we have provided a specific code list that can be reached on Gitlab. For the IPI, we do not have a code list, as there is no Implementing Act yet.
* FR: Is it possible to link the guide with the code list?
* EC: Yes, this is possible that we link the code list and guide. We will investigate this.
* AT: Is it possible to align the wording to mention tenderer instead of company.
* EC: We will update the wording from company to tenderer in the code list of the FSR.
* FR: Is it possible that there is an explanation provided on the fields that are “Conditional Mandatory for guides?
* EC: We have included this information in the guides under the business rules. We will continue to use the guides to provide the rules in this way to make it easier for the users to understand.

## Exclusion and Selection Criteria Guide

* DE: I am unsure about the same system used in exclusion grounds and selection criteria, as they are very different. Exclusion grounds are mostly by law and mostly the same (based on the Directives) and don't need to be repeated in each notice whereas the selection criteria are very specific to the respective procurement and a lot will not be able to fit within a code list.
* EC: There is a lot of flexibility on how it can be implemented, the guide provides some guide for countries how to implement it. For example, buyers can indicate that the exclusion grounds can be provided in the procurement documents, while selection criteria can be provided in the notices. Therefore, it is a question on how this is tailored.
* SE: We would like to know more about the implementation of all the possibilities in the notice.
* EC: The countries can decide on this aspect. Regarding the selection criteria, they are applied for each lot. If the same selection criteria are applied for the 10 lots, in eForms, this needs to be provided 10 times. Nevertheless, the eProcurement system can and should hide this from the buyer to make it easier for them.
* IE: Is it possible to have trigger fields added to annex? It would be used for the developers.
* EC: We will investigate this.
* DE (KoSIT): As a developer I would be happy with the descriptive rules. For the ESC guide: define the code list for Selection Criterion BT-809, which would be a subset of https://op.europa.eu/en/web/eu-vocabularies/concept-scheme/-/resource?uri=http://publications.europa.eu/resource/authority/criterion
* EC: Please review the guide about exclusion grounds and selection criteria (ESC), by 26 April.
* FR: Why do we have so much information included in the annex for review, as there is so little requested under the Directive on this aspect.
* EC: There needs to be a discussion and guide for this. We will need your input.
* FR: My input would be that we restrict ourselves to what is required in the Directives.
* LV: According to the Excel sheet both Exclusion Grounds and Selection Criteria (BG-700) and Review (BG 716) are optional. Are there any changes planned regarding BG-700? Do I understand correctly that all fields of BG-700 and BG-716 are not mandatory if the Member States choose not to include any of these fields in Notices.
* EC: It is an error for BG-700 to be optional, as they are mandatory by the Directives. On the other hand, Review (BG-716) is indeed optional. So here, countries can choose not to include it.

## Voluntary notices

* EC: It would be important that MS who has already implemented voluntary notices, please provide your input under issue 40 on Gitlab. We will organize a dedicated workshop on voluntary forms.

## Governance model

* EC: We have published the governance document in 2020. It was never used. We would like to revise the governance model and discuss it with you.
* NL: Where would you position the ACPC? Is it more political than EXPP?
* FR: Would the eForms amendment be discussed during the EXPP?
* EC: We believe it is still important to discuss eForms with EXPP, but ACPC is the body for the new amendment. As we revise the Annex, we will discuss this during the eForms workshops.

## Software Development Toolkit (SDK) 

* NL: My concern is related to the planning of the SDK. We had 6 months for the implementation. In our view, you have to make sure that for SDK 1.13 there will not be a delay.
* EC: We are providing guides, and we believe we might have more meetings on the specific topics. The idea is that we discuss new topics like FSR in this forum before we start the implementation. For the SDK release 1.12 in June, we are on track.
* SE: How about allowing contributing bugfixes on existing SDK from developers, because in my personal experience updates/bugfixes takes too much time.
* DE (KoSIT): Unfortunately contributing bug fixes is hampered by the fact that most elements happen in a closed database. Hence if we supply a bug fix it cannot be merged/pulled in automatically. It relates to the issue of overall architecture.
* SE: It would be great to have an update from EXPP as there is much more focus on sectorial legislation.
* FI: Legal and technical elements MUST work in parallel. Otherwise, we might have legal elements that are difficult to implement. Alternatively, the Commission does not get the information from TED that they imagine they would get (I am particularly referring to EED fields in eForms).
* NL: Stakeholder management from OP side is missing: What will you do? What would be a focus in the future?  It would be also important for the MS to be able to contact each other. When it comes to EXPP, there is a contact list, it is handy to have these contacts. Is it possible to create such a contact list?
* SE: If possible, it is necessary to mention to other DGs what eForms is and how it works. This could help with sectorial legislation.
* EC: We are in touch with other DGs.
* FR: Could you please provide what will be included in the 1.13 for the SDK for the EED?
* EC: We have updated the Excel to show what will be part of 1.12 and 1.13.
* DK: How confident are you, the SDK 1.13 will be ready by October and that voluntary forms will be included?
* NL: October is kind of important to pull this off for the mandatory deadline.
* EC: SDK 1.13 has indeed a lot of new features. If we are able to keep up the pace like for SDK 1.12 and if we get also input from your side on the voluntary forms, we will make it.
* LU: Are other mandatory technical updates foreseen besides the SDKs, like the web services?
* OP: We do plan to improve the APIs we offer, like having consistent URLs, but the existing ones will continue to work in parallel until everyone has a chance to move.

## SDK - translations

* CY: What about countries that have the same language. How can we coordinate the translation (e.g. Greece and Cyprus).
* EC: It is important that those countries coordinate themselves.
* DE: In which time frames is it possible to provide feedback on the translations into national languages to be considered in SDK 1.12 or 1.13?
* OP: For translations, we're currently targetting SDK 1.13 for the next round of updates - so we'd need input by September at the latest.

## Closing

### Feedback

* FI: PPDS and eForms should be looked together. Good to combine those in September.
* DE (KoSIT): Let us continue the new way of working.
* LU: Hello, this is feedback to what you said about the meeting in September: it will still be better to do it in virtual and physical
* OP: The feedback in these workshops is essential to get to good implementations.
* SK: The meeting in Tirana is great, but I'd like to know if SDK 1.7 will be still functioning after September 2024.
